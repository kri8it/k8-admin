## CHANGELOG

= 1.0 =
* Initial release

= 1.0.3 =
* K8 Updater compatibility

= 1.0.6 =
* Navi big logo option added

= 1.0.7 =
* Gravity Form button option added
* iBox Square image option