# K8 Admin #

* Contributors: charl@kri8it.com
* Donate link: http://kri8it.com/
* Tags: admin
* Requires at least: 3.0.1
* Tested up to: 4.0
* Stable tag: 4.3
* License: GPLv2 or later
* License URI: http://www.gnu.org/licenses/gpl-2.0.html

Clean up admin for our users and different user roles.

## Description ##

This plugin manages the admin and front-end page builder capabilities of our user roles.

## Installation ##

1. Upload the `k8-admin` folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress


## Frequently Asked Questions ##

### A question that someone might have ###

An answer to that question.

### What about foo bar? ###

Answer to foo bar dilemma.


### Upgrade Notice ###

**1.0**
Upgrade notices describe the reason a user should upgrade.  No more than 300 characters.

**0.5**
This version fixes a security related bug.  Upgrade immediately.

### Arbitrary section ###

You may provide arbitrary sections, in the same format as the ones above.  This may be of use for extremely complicated
plugins where more information needs to be conveyed that doesn't fit into the categories of "description" or
"installation."  Arbitrary sections will be shown below the built-in sections outlined above.