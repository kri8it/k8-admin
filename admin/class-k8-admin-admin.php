<?php

/**
 * The dashboard-specific functionality of the plugin.
 *
 * @link       http://kri8it.com
 * @since      1.0.0
 *
 * @package    K8_Admin
 * @subpackage K8_Admin/admin
 */

/**
 * The dashboard-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the dashboard-specific stylesheet and JavaScript.
 *
 * @package    K8_Admin
 * @subpackage K8_Admin/admin
 * @author     Charl Pretorius <charl@kri8it.com>
 */
class K8_Admin_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $K8_Admin    The ID of this plugin.
	 */
	private $K8_Admin;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;
	
	/**
	 * The user role class of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      class    $K8_Admin_User_Roles    The user role class of this plugin.
	 */
	private $K8_Admin_User_Roles;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @var      string    $K8_Admin       The name of this plugin.
	 * @var      string    $version    The version of this plugin.
	 */
	public function __construct( $K8_Admin, $version, $K8_Admin_User_Roles ) {
						
		$this->K8_Admin = $K8_Admin;
		$this->version = $version;
		
		$this->K8_Admin_User_Roles = $K8_Admin_User_Roles;
		
	}
	
	/**
	 * Get Current User Role
	 */
	public function get_current_user_role(){
		
		$this->K8_Admin_User_Roles->set_current_user_role();
		
	}
	
	/**
	 * Register Custom Post Types
	 * 
	 * @since    1.0.0
	 */
	public function register_post_types(){
				
		if( pl_setting( 'k8_site_post_type_portfolio' ) ):
		
			/* Portfolio Post Type -------------------------------------------------------------- */
		
			$labels = array(
				'name' => _x( 'Portfolios', 'post type general name', 'k8' ),
				'singular_name' => _x( 'Portfolio Item', 'post type singular name', 'k8' ),
				'add_new' => _x( 'Add New', 'portfolio', 'k8' ),
				'add_new_item' => __( 'Add Portfolio Item', 'k8' ),
				'edit_item' => __( 'Edit Portfolio Item', 'k8' ),
				'new_item' => __( 'New Portfolio Item', 'k8' ),
				'view_item' => __( 'View Portfolio Item', 'k8' ),
				'search_items' => __( 'Search Portfolios', 'k8' ),
				'not_found' =>  __( 'No Portfolios found', 'k8' ),
				'not_found_in_trash' => __( 'No Portfolios found in Trash', 'k8' ), 
				'parent_item_colon' => ''
			);
			
			$rewrite = 'portfolio';
			
			$args = array(
				'labels' => $labels,
				'public' => true,
				'publicly_queryable' => true,
				'show_ui' => true, 
				'query_var' => true,
				'rewrite' => array( 'slug' => $rewrite ),
				'capability_type' => 'post',
				'hierarchical' => false,
				'menu_icon' => 'dashicons-awards',
				'menu_position' => null, 
				'has_archive' => true, 
				'taxonomies' => array( 'category', 'portfolio-category' ), 
				'supports' => array( 'title','editor','thumbnail','page-attributes','author','excerpt' )
			);
			
			register_post_type( 'portfolio', $args );
			
			/* "Portfolio Category" Custom Taxonomy ---------------------------------------------------------------------- */
			$labels = array(
				'name' => _x( 'Category', 'taxonomy general name', 'k8' ),
				'singular_name' => _x( 'Category', 'taxonomy singular name','k8' ),
				'search_items' =>  __( 'Search Categories', 'k8' ),
				'all_items' => __( 'All Categories', 'k8' ),
				'parent_item' => __( 'Parent Categories', 'k8' ),
				'parent_item_colon' => __( 'Parent Category:', 'k8' ),
				'edit_item' => __( 'Edit Category', 'k8' ), 
				'update_item' => __( 'Update Category', 'k8' ),
				'add_new_item' => __( 'Add New Category', 'k8' ),
				'new_item_name' => __( 'New Category Name', 'k8' ),
				'menu_name' => __( 'Categories', 'k8' )
			); 	
			
			$args = array(
				'hierarchical' => true,
				'labels' => $labels,
				'show_ui' => true,
				'query_var' => true,
				'rewrite' => array( 'slug' => 'portfolio-category' )
			);
			
			register_taxonomy( 'portfolio-category', array( 'portfolio' ), $args );
		
		endif;
		
		if( pl_setting( 'k8_site_post_type_team' ) ):
		
			/* Team Post Type -------------------------------------------------------------- */
			
			$labels = array(
				'name' => _x( 'Team', 'post type general name', 'k8' ),
				'singular_name' => _x( 'Team Member', 'post type singular name', 'k8' ),
				'add_new' => _x( 'Add New', 'portfolio', 'k8' ),
				'add_new_item' => __( 'Add Team Member', 'k8' ),
				'edit_item' => __( 'Edit Team Member', 'k8' ),
				'new_item' => __( 'New Team Member', 'k8' ),
				'view_item' => __( 'View Team Member', 'k8' ),
				'search_items' => __( 'Search Team Members', 'k8' ),
				'not_found' =>  __( 'No Team Members found', 'k8' ),
				'not_found_in_trash' => __( 'No Team Members found in Trash', 'k8' ), 
				'parent_item_colon' => ''
			);
			
			$rewrite = 'team';
			
			$args = array(
				'labels' => $labels,
				'public' => true,
				'publicly_queryable' => true,
				'show_ui' => true, 
				'query_var' => true,
				'rewrite' => array( 'slug' => $rewrite ),
				'capability_type' => 'post',
				'hierarchical' => false,
				'menu_icon' => 'dashicons-groups',
				'menu_position' => null, 
				'has_archive' => true,		
				'supports' => array( 'title','editor','thumbnail','page-attributes','author','excerpt' )
			);
					
			register_post_type( 'team', $args );
			
			/* "Team Category" Custom Taxonomy ---------------------------------------------------------------------- */
			$labels = array(
				'name' => _x( 'Category', 'taxonomy general name', 'k8' ),
				'singular_name' => _x( 'Category', 'taxonomy singular name','k8' ),
				'search_items' =>  __( 'Search Categories', 'k8' ),
				'all_items' => __( 'All Categories', 'k8' ),
				'parent_item' => __( 'Parent Categories', 'k8' ),
				'parent_item_colon' => __( 'Parent Category:', 'k8' ),
				'edit_item' => __( 'Edit Category', 'k8' ), 
				'update_item' => __( 'Update Category', 'k8' ),
				'add_new_item' => __( 'Add New Category', 'k8' ),
				'new_item_name' => __( 'New Category Name', 'k8' ),
				'menu_name' => __( 'Categories', 'k8' )
			); 	
			
			$args = array(
				'hierarchical' => true,
				'labels' => $labels,
				'show_ui' => true,
				'query_var' => true,
				'rewrite' => array( 'slug' => 'team-category' )
			);
			
			register_taxonomy( 'team-category', array( 'team' ), $args );
		
		endif;
		
		if( pl_setting( 'k8_site_post_type_gallery' ) ):
		
			/* Gallery Post Type -------------------------------------------------------------- */
			
			$labels = array(
				'name' => _x( 'Gallery', 'post type general name', 'k8' ),
				'singular_name' => _x( 'Gallery Item', 'post type singular name', 'k8' ),
				'add_new' => _x( 'Add New', 'gallery', 'k8' ),
				'add_new_item' => __( 'Add Gallery Item', 'k8' ),
				'edit_item' => __( 'Edit Gallery Item', 'k8' ),
				'new_item' => __( 'New Gallery Item', 'k8' ),
				'view_item' => __( 'View Gallery Item', 'k8' ),
				'search_items' => __( 'Search Galleries', 'k8' ),
				'not_found' =>  __( 'No Galleries found', 'k8' ),
				'not_found_in_trash' => __( 'No Galleries found in Trash', 'k8' ), 
				'parent_item_colon' => ''
			);
			
			$rewrite = 'gallery';
			
			$args = array(
				'labels' => $labels,
				'public' => true,
				'publicly_queryable' => true,
				'show_ui' => true, 
				'query_var' => true,
				'rewrite' => array( 'slug' => $rewrite ),
				'capability_type' => 'post',
				'hierarchical' => false,
				'menu_icon' => 'dashicons-format-gallery',
				'menu_position' => null, 
				'has_archive' => true, 
				'taxonomies' => array( 'gallery-category' ), 
				'supports' => array( 'title','editor','thumbnail' )
			);
					
			register_post_type( 'gallery', $args );
			
			/* "Gallery Category" Custom Taxonomy ---------------------------------------------------------------------- */
			$labels = array(
				'name' => _x( 'Category', 'taxonomy general name', 'k8' ),
				'singular_name' => _x( 'Category', 'taxonomy singular name','k8' ),
				'search_items' =>  __( 'Search Categories', 'k8' ),
				'all_items' => __( 'All Categories', 'k8' ),
				'parent_item' => __( 'Parent Categories', 'k8' ),
				'parent_item_colon' => __( 'Parent Category:', 'k8' ),
				'edit_item' => __( 'Edit Category', 'k8' ), 
				'update_item' => __( 'Update Category', 'k8' ),
				'add_new_item' => __( 'Add New Category', 'k8' ),
				'new_item_name' => __( 'New Category Name', 'k8' ),
				'menu_name' => __( 'Categories', 'k8' )
			); 	
			
			$args = array(
				'hierarchical' => true,
				'labels' => $labels,
				'show_ui' => true,
				'query_var' => true,
				'rewrite' => array( 'slug' => 'gallery-category' )
			);
			
			register_taxonomy( 'gallery-category', array( 'gallery' ), $args );
		
		endif; 
		
	}
	
	/**
	 * Hide Custom Meta Boxes in wp-admin/edit.php
	 *  - postcustom
	 *  - sharing_meta
	 * 
	 * @since    1.0.0
	 */	 
	public function remove_custom_meta_boxes() {
			
	    $user = wp_get_current_user();
		
		$client_admin = array( $this->K8_Admin_User_Roles->client_admin_role );
		
		/* Client Admin User Role */
		if( $this->K8_Admin_User_Roles->is_client_admin() ):
			
			$hide_meta_boxes = array( 
				'postcustom' 		=> 'normal', 
				'sharing_meta' 		=> 'advanced', 
				'trackbacksdiv' 	=> 'normal', 
				'commentstatusdiv' 	=> 'normal', 
				'slugdiv' 			=> 'normal'
			);
			
		endif;
		
		/* Manager Admin User Role */
		if( $this->K8_Admin_User_Roles->is_manager_admin() ):
			
			$hide_meta_boxes = array( 
				'postcustom' 		=> 'normal', 
				'sharing_meta' 		=> 'advanced'								
			);
			
		endif;
		
		foreach( $hide_meta_boxes as $box => $context ):
			
			remove_meta_box( $box, 'post', $context );
	    	remove_meta_box( $box, 'page', $context );
			remove_meta_box( $box, 'product', $context );
			remove_meta_box( $box, 'team', $context );
			remove_meta_box( $box, 'portfolio', $context );
			remove_meta_box( $box, 'feedback', $context );
			remove_meta_box( $box, 'gallery', $context );
			remove_meta_box( $box, 'shop_order', $context );
			remove_meta_box( $box, 'shop_coupon', $context );
			
		endforeach;
		
	}
	
	/**
	 * Hide Admin Area Menus  
	 * 
	 * @since    1.0.0
	 */	
	public function remove_menu_items(){
		
		if( $this->K8_Admin_User_Roles->is_client_admin() ){ // client_admin role
			
			remove_menu_page( 'link-manager.php' );		
			remove_menu_page( 'tools.php' );
			remove_submenu_page( 'themes.php', 'themes.php' );
			remove_menu_page( 'jetpack' );
			remove_menu_page( 'PageLines-Admin' );
			remove_menu_page( 'options-general.php' );
			remove_submenu_page( 'themes.php', 'customize.php?return=%2Fwp-admin%2Findex.php' );
			remove_submenu_page( 'themes.php', 'customize.php?return=%2Fwp-admin%2F' );
	
		}
		
		if( $this->K8_Admin_User_Roles->is_manager_admin() ){ // manager_admin role
								
			remove_menu_page( 'link-manager.php' );		
			remove_menu_page( 'tools.php' );
			remove_submenu_page( 'themes.php', 'themes.php' );
			remove_menu_page( 'jetpack' );
			remove_menu_page( 'PageLines-Admin' );
			remove_menu_page( 'options-general.php' );
			remove_submenu_page( 'themes.php', 'customize.php?return=%2Fwp-admin%2Findex.php' );
			remove_submenu_page( 'themes.php', 'customize.php?return=%2Fwp-admin%2F' );
	
		}
		
	}
	
	/**
	 * Remove Admin Dashboard Widgets
	 * 
	 * @since    1.0.0
	 */
	public function remove_dashboard_widgets(){
			
		if( $this->K8_Admin_User_Roles->is_client_admin() ){ // client_admin role
				  	
			global$wp_meta_boxes;
			
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
			unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
			unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']); 
			unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
			unset($wp_meta_boxes['dashboard']['side']['core']['yoast_db_widget']);
			unset($wp_meta_boxes['dashboard']['normal']['core']['bruteprotect_dashboard_widget']);
			
		}
  		
		if( $this->K8_Admin_User_Roles->is_manager_admin() ){ // manager_admin role
				  	
			global$wp_meta_boxes;
			
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
			unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
			unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']); 
			unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
			unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
			unset($wp_meta_boxes['dashboard']['side']['core']['yoast_db_widget']);
			unset($wp_meta_boxes['dashboard']['normal']['core']['bruteprotect_dashboard_widget']);
			
		}
		
	}
	
	/**
	 * Remove Admin Bar Links
	 * 
	 * @since    1.0.0
	 */
	public function remove_admin_bar_links() {
		
		if( $this->K8_Admin_User_Roles->is_client_admin() ){ // client_admin role
				  
			global $wp_admin_bar;
			
			$wp_admin_bar->remove_menu('my-blogs');
			$wp_admin_bar->remove_menu('my-account-with-avatar');
			$wp_admin_bar->remove_menu('new-link');	
			$wp_admin_bar->remove_menu('wp-logo');
			$wp_admin_bar->remove_menu('my-sites');	
			$wp_admin_bar->remove_menu('appearance');			
			$wp_admin_bar->remove_menu('actionmap');
			$wp_admin_bar->remove_menu('page_template');
			$wp_admin_bar->remove_menu('notes');
			
		}

		if( $this->K8_Admin_User_Roles->is_manager_admin() ){ // manager_admin role
				  
			global $wp_admin_bar;
			
			$wp_admin_bar->remove_menu('my-blogs');
			$wp_admin_bar->remove_menu('my-account-with-avatar');
			$wp_admin_bar->remove_menu('new-link');	
			$wp_admin_bar->remove_menu('wp-logo');
			$wp_admin_bar->remove_menu('my-sites');	
			$wp_admin_bar->remove_menu('appearance');			
			$wp_admin_bar->remove_menu('actionmap');
			$wp_admin_bar->remove_menu('page_template');
			$wp_admin_bar->remove_menu('notes');
			
		}

	}
	
	/**
	 * Remove Yoast Admin Columns
	 *
	 * @since    1.0.0
	 */
	public function yoast_admin_cols() {
		
		if( $this->K8_Admin_User_Roles->is_client_admin() ){
			return false;
		}
		
	}
	
	/**
	 * Remove ACF Admin Menu
	 * 
	 * @since	1.0.5	
	 */
	public function acf_capability_setting( $capability ){
		
		return 'activate_plugins';
		
	}
	
	/**
	 * Add global DMS options
	 *
	 * @since    1.0.0
	 */
	public function global_dms_options() {
		
		$my_theme_settings = array();
		$my_theme_settings['k8_global'] = array(
		    'pos'   => 50,                  // Tab position (lower means higher)
		    'name'  => 'K8 Options',     	// Tab name
		    'icon'  => 'icon-cubes',    	// See 'font awesome'
		    'opts'  => array(               // Option Engine Format...
	        	array(
					'title'			=> __('Theme Options', 'pagelines'),
					'type'			=> 'multi',
					'opts'			=> array(						
						
				        array(
				            'key'           => 'k8_opts_logo_navi_logo_big',
				            'title'         => __('Navi Logo Size', 'pagelines'),
				            'label'         => __('Enable Bigger Navi Logo', 'pagelines'),
			            	'type'          => 'check',
			            	'help'			=> 'Note: The actual uploaded image dimensions will be used to size the logo'			                       
				        ),
				        
						array(
				            'key'           => 'k8_opts_gform_button_theme',
				            'title'         => __('Gravity Form Submit Button Theme', 'pagelines'),
				            'type'          => 'select_button'			            			                       
				        ),
				        
						array(
				            'key'           => 'k8_opts_ibox_square_thumbnails',
				            'title'         => __('iBox - Enable Square Thumbnails', 'pagelines'),
				            'type'          => 'check',
				            'help'			=> 'If you want only a specific iBox section with square images add "square" to the Custom Styling Classes on the section options.'		            			                       
				        )				        
						
					)
				)
		    )
		); 
		
		if( $this->K8_Admin_User_Roles->is_main_admin() ){
			
			$default_pages = 8;
			
			if( pl_setting( 'k8_site_level' ) == 'pro' ):
				
				$default_pages = 8;
				
			elseif( pl_setting( 'k8_site_level' ) == 'premium' ):
				
				$default_pages = 10;
				
			elseif( pl_setting( 'k8_site_level' ) == 'ultimate' ):
				
				$default_pages = 12;
				
			endif;
				
			$my_theme_settings['k8_site_settings'] = array(
			    'pos'   => 50,                  	// Tab position (lower means higher)
			    'name'  => 'K8 Site Settings',     	// Tab name
			    'icon'  => 'icon-cogs',    			// See 'font awesome'
			    'opts'  => array(               	// Option Engine Format...
			        
			        array(
						'title'			=> __('Site Settings', 'pagelines'),
						'type'			=> 'multi',
						'opts'			=> array(
							
							array(
					            'key'           => 'k8_site_level',
					            'label'         => __('Site Level', 'pagelines'), 
				            	'type'          => 'select', 
					            'opts'=> array(
					                'pro'          => array( 'name' => 'Professional' ),
					                'premium'      => array( 'name' => 'Premium' ),
					                'ultimate'     => array( 'name' => 'Ultimate' )
					            ),
					            'default' 		=> 'pro'            
					        ),
					        array(
					            'key'           => 'k8_modules_heading',
					            'label'         => __('Modules', 'pagelines'),
				            	'type'          => 'template', 
					            'template'      => '<legend>Modules</legend><p>Below you can disable and enable modules</p>'	            
					        ),
					        array(
					            'key'           => 'k8_site_module_branch_locator',
					            'title'         => __('Branch Locator', 'pagelines'),
					            'label'         => __('Enable Branch Locator', 'pagelines'),
				            	'type'          => 'check'			                       
					        ),
					        array(
					            'key'           => 'k8_site_module_bookings',
					            'title'         => __('Bookings', 'pagelines'),
					            'label'         => __('Enable Bookings', 'pagelines'),
				            	'type'          => 'check'			                       
					        ),
					        array(
					            'key'           => 'k8_site_module_appointments',
					            'title'         => __('Appointments', 'pagelines'), 
					            'label'         => __('Enable Appointments', 'pagelines'), 
				            	'type'          => 'check'			                       
					        ),
							
						)
					),
					
					array(
					
						'title'			=> __('Page & Post Type Settings', 'pagelines'),
						'type'			=> 'multi',
						'col'			=> 2,
						'opts'			=> array(
						
							array(
					            'key'           => 'k8_site_page_count',
					            'label'         => __('Number of Pages', 'pagelines'), 
					            'type'          => 'count_select', 
					            'count_start'   => 8,               // Starting Count Number
					            'count_number'  => 100,             // Ending Count Number
					            'default'		=> $default_pages,	
					            'help'			=> 'This site currently has ' . $this->get_page_count() . ' published pages.'				            
					        ),
					        array(
					            'key'           => 'k8_post_types_heading',
					            'label'         => __('Post Types', 'pagelines'),
				            	'type'          => 'template', 
					            'template'      => '<legend>Post Types</legend><p>Below you can disable and enable post types</p>'	            
					        ),
					        array(
					            'key'           => 'k8_site_post_type_portfolio',
					            'title'         => __('Portfolio', 'pagelines'),
					            'label'         => __('Enable the Portfolio', 'pagelines'),
				            	'type'          => 'check'			                       
					        ),
					        array(
					            'key'           => 'k8_site_post_type_team',
					            'title'         => __('Team', 'pagelines'), 
					            'label'         => __('Enable the Team', 'pagelines'), 
				            	'type'          => 'check'			                       
					        ),
					        array(
					            'key'           => 'k8_site_post_type_gallery',
					            'title'         => __('Gallery', 'pagelines'), 
					            'label'         => __('Enable the Gallery', 'pagelines'), 
				            	'type'          => 'check'			                       
					        ),
						
						)
					
					)
			        			        
			    )
			);
			
		}
		
		pl_add_theme_tab( $my_theme_settings );
		
	}

	/**
	 * Toggle DMS Section Modules via our options panel
	 * 
	 * @since    1.0.0
	 */
	public function toggle_section_modules(){
		
		/**
		 * Global Option Enable or Disable Modules
		 */
		
		/* Branch Locator Module */
		( pl_setting( 'k8_site_module_branch_locator' ) ) ? pl_setting_update( 'disable-K8BranchLocator', false ) : pl_setting_update( 'disable-K8BranchLocator', true );
		
		
		/**
		 * Disable some basic secitons based on site level
		 */
		
		$disable_sections = $this->site_level_section_restrictions();
		
		foreach( $disable_sections as $section ):
			
			pl_setting_update( 'disable-' . $section, true );
			
		endforeach;
				
	}
	
	/**
	 * Disable Sections based on site level
	 * 
	 * @since    1.0.0 
	 */
	protected function site_level_section_restrictions(){
		
		$disable_array = array();
		$premium_disable_array = array();
		$pro_disable_array = array();
		
		/**
		 * Set up sections to be disabled
		 */
		
		$premium_disable_array[] = 'PLNextBox';
		$premium_disable_array[] = 'PageLinesStarBars';
		
		$pro_disable_array[] = 'K8BranchLocator';
		$pro_disable_array[] = 'K8GForm';
		$pro_disable_array[] = 'PLPopShot';	
		$pro_disable_array[] = 'PLProPricing';
		$pro_disable_array[] = 'PLQuickCarousel';
		$pro_disable_array[] = 'PageLinesQuickSlider';
		$pro_disable_array[] = 'PLRapidTabs';
		$pro_disable_array[] = 'plRevSlider';
		$pro_disable_array[] = 'ScrollSpy';
		$pro_disable_array[] = 'PageLinesSecondNav';
		$pro_disable_array[] = 'PLTestimonials';
		
		/**
		 * Premium
		 */ 
		if( pl_setting( 'k8_site_level' ) == 'premium' ):
			
			return apply_filters( 'k8_admin_site_level_sections_disable', $premium_disable_array );
						
		endif;
		
		/**
		 * Proffesional
		 */
		if( pl_setting( 'k8_site_level' ) == 'pro' ):
			
			$pro_disable_array = array_merge( $pro_disable_array, $premium_disable_array );
			
			return apply_filters( 'k8_admin_site_level_sections_disable', $pro_disable_array );
				
		endif;
		
	} 
	 
	 
	/**
	 * Get current published page count
	 * 
	 * @since    1.0.0
	 */
	public function get_page_count(){
		
		return wp_count_posts( 'page' )->publish;
		
	}
	
	/**
	 * Limit the number of pages
	 *
	 * @since    1.0.0
	 */
	public function limit_pages( $post_id, $post, $update ){
		
		$number_of_pages = pl_setting( 'k8_site_page_count' );
		$current_page_count = $this->get_page_count();
		
		if( $current_page_count > $number_of_pages ){
			
			$post->post_status = 'draft';
			wp_update_post( $post );
			
			add_filter( 'redirect_post_location', array( $this, 'add_notice_query_var' ), 99 );
			
		}
		
	}
	
	/**
	 * Page Limit Notification Query Var
	 * 
	 * @since    1.0.0
	 */
	public function add_notice_query_var( $location ) {
   	
		remove_filter( 'redirect_post_location', array( $this, 'add_notice_query_var' ), 99 );
   		return add_query_arg( array( 'k8_page_limit' => '1' ), $location );
		
  	}
	
	/**
	 * Page Limit Notification
	 * 
	 * @since    1.0.0
	 */
	public function page_limit_notice(){
		
		if ( ! isset( $_GET['k8_page_limit'] ) ) return;
		
		?>
	    <div class="error">
	        <p><?php _e( 'You have reached your page limit. Please upgrade your package or order more pages with a page bundle.', 'k8' ); ?></p>
	        <p><?php _e( 'For now we have set this page to draft.', 'k8' ); ?></p>
	        <p><a target="_blank" class="button" href="#"><?php _e( 'Click here to order more pages now!', 'k8' ); ?></a></p>
	    </div>
	    <?php
		
	}
	
	/**
	 * Register the stylesheets for the Dashboard.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in K8_Admin_Admin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The K8_Admin_Admin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->K8_Admin, plugin_dir_url( __FILE__ ) . 'css/k8-admin-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the dashboard.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in K8_Admin_Admin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The K8_Admin_Admin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->K8_Admin, plugin_dir_url( __FILE__ ) . 'js/k8-admin-admin.js', array( 'jquery' ), $this->version, false );

	}
			 
	

}
