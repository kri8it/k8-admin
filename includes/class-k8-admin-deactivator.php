<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://kri8it.com
 * @since      1.0.0
 *
 * @package    K8_Admin
 * @subpackage K8_Admin/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    K8_Admin
 * @subpackage K8_Admin/includes
 * @author     Charl Pretorius <charl@kri8it.com>
 */
class K8_Admin_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
