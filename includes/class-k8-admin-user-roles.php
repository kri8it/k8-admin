<?php

/**
 * Define the user roles used by the plugin.
 *
 * @link       http://kri8it.com
 * @since      1.0.0
 *
 * @package    K8_Admin
 * @subpackage K8_Admin/includes
 */

/**
 * Define the user roles used by the plugin.
 *
 * Defines the user roles available to users and administrators
 *
 * @package    K8_Admin
 * @subpackage K8_Admin/includes
 * @author     Charl Pretorius <charl@kri8it.com>
 */
class K8_Admin_User_Roles {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $K8_Admin    The ID of this plugin.
	 */
	private $K8_Admin;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;
			
	/**
	 * The client admin user role of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $client_admin_role    The client admin user role of this plugin.
	 */
	public $client_admin_role;
	
	/**
	 * The manager admin user role of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $manager_admin_role    The manager admin user role of this plugin.
	 */
	public $manager_admin_role;
	
	/**
	 * The main admin user role of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $main_admin_role    The main admin user role of this plugin.
	 */
	public $main_admin_role;
	
	/**
	 * The current user role of the logged in user.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $current_user_role    The current user role of the logged in user.
	 */
	public $current_user_role;
	
	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @var      string    $K8_Admin       The name of this plugin.
	 * @var      string    $version    The version of this plugin.
	 */
	public function __construct( $K8_Admin, $version ) {

		$this->K8_Admin = $K8_Admin;
		$this->version = $version;
		
		/* Set user roles - TODO create admin interface for this */
		$this->main_admin_role 		= 'administrator';
		$this->manager_admin_role 	= 'manager_admin';
		$this->client_admin_role	= 'client_admin';		
		
	}
	
	/**
	 * Set Current User Role
	 * 
	 * @since	1.0.0
	 */
	public function set_current_user_role(){								
				
		if( $this->check_user_role( $this->main_admin_role ) ):
		
			$this->current_user_role = $this->main_admin_role;
		
		elseif( $this->check_user_role( $this->manager_admin_role ) ):
		
			$this->current_user_role = $this->manager_admin_role;
		
		elseif( $this->check_user_role( $this->client_admin_role ) ):
		
			$this->current_user_role = $this->client_admin_role;
			
		else:
			
			$this->current_user_role = 'other';
						
		endif;
		
	}
	
	/**
	 * Get Current User Role
	 * 
	 * @since	1.0.0
	 */
	public function get_current_user_role(){								
				
		return $this->current_user_role;
		
	}
	
	/**
	 * Checks if a particular user has a role. 
	 * Returns true if a match was found.
	 *
	 * @since    1.0.0
	 * 
	 * @param 	string $role Role name.
	 * @param 	int $user_id (Optional) The ID of a user. Defaults to the current user.
	 * @return 	bool
	 */
	public function check_user_role( $role, $user_id = null ) {
	 
	    if ( is_numeric( $user_id ) )
			$user = get_userdata( $user_id );
	    else
	        $user = wp_get_current_user();
	 
	    if ( empty( $user ) )
			return false;
	 
	    return in_array( $role, (array) $user->roles );
	}
	
	/**
	 * Check if current role is Client Admin User Role
	 * 
	 * @since	1.0.0
	 */
	public function is_client_admin(){								
				
		return $this->check_user_role( $this->client_admin_role );
		
	}
	
	/**
	 * Check if current role is Manager Admin User Role
	 * 
	 * @since	1.0.0
	 */
	public function is_manager_admin(){								
				
		return $this->check_user_role( $this->manager_admin_role );
		
	}

	/**
	 * Check if current role is Main Admin User Role
	 * 
	 * @since	1.0.0
	 */
	public function is_main_admin(){								
				
		return $this->check_user_role( $this->main_admin_role );
		
	}

}
