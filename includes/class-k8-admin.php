<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the dashboard.
 *
 * @link       http://kri8it.com
 * @since      1.0.0
 *
 * @package    K8_Admin
 * @subpackage K8_Admin/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, dashboard-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    K8_Admin
 * @subpackage K8_Admin/includes
 * @author     Charl Pretorius <charl@kri8it.com>
 */
class K8_Admin {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      K8_Admin_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $K8_Admin    The string used to uniquely identify this plugin.
	 */
	protected $K8_Admin;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;
	
	/**
	 * The user role class of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      class    $K8_Admin_User_Roles    The user role class of this plugin.
	 */
	private $K8_Admin_User_Roles;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the Dashboard and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
								
		$this->K8_Admin = 'k8-admin';
		$this->version = '1.0.0';
				
		$this->load_dependencies();
		$this->set_roles();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}
	
	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - K8_Admin_Loader. Orchestrates the hooks of the plugin.
	 * - K8_Admin_i18n. Defines internationalization functionality.
	 * - K8_Admin_Admin. Defines all hooks for the dashboard.
	 * - K8_Admin_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-k8-admin-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-k8-admin-i18n.php';
		
		/**
		 * The class responsible for defining the custom user roles and realted actions
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-k8-admin-user-roles.php';
		
		/**
		 * The class responsible for defining all actions that occur in the Dashboard.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-k8-admin-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-k8-admin-public.php';

		$this->loader = new K8_Admin_Loader();
				
	}

	/**
	 * Define the user roles and get current admin role id any
	 *
	 * Uses the K8_Admin_User_Roles class in order to set the user roles
	 * with DMS Editor.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_roles() {
		
		$this->K8_Admin_User_Roles = new K8_Admin_User_Roles( $this->K8_Admin, $this->version );
		$plugin_roles = $this->K8_Admin_User_Roles;
		
		$this->loader->add_action( 'plugins_loaded', $plugin_roles, 'set_current_user_role' );
		
	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the K8_Admin_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new K8_Admin_i18n();
		$plugin_i18n->set_domain( $this->get_K8_Admin() );
		
		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the dashboard functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new K8_Admin_Admin( $this->get_K8_Admin(), $this->get_version(), $this->K8_Admin_User_Roles );
		
		$this->loader->add_action( 'plugins_loaded', $plugin_admin, 'get_current_user_role' );
		
		/* Register Post Types */
		$this->loader->add_action( 'init', $plugin_admin, 'register_post_types' );
		
		/* Remove Unwanted Meta Boxes */
		$this->loader->add_action( 'add_meta_boxes', $plugin_admin, 'remove_custom_meta_boxes' );
		
		/* Clean Up Admin Area Menus */
		$this->loader->add_action( 'admin_init', $plugin_admin, 'remove_menu_items' );
		
		/* Clean Up Admin Dashboard */
		$this->loader->add_action( 'wp_dashboard_setup', $plugin_admin, 'remove_dashboard_widgets' );
		
		/* Clean Up Admin Bar Links */
		$this->loader->add_action( 'wp_before_admin_bar_render', $plugin_admin, 'remove_admin_bar_links' );
		
		/* Admin CSS and JS */
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		
		/* Remove Yoast SEO Admin Columns */
		$this->loader->add_filter( 'wpseo_use_page_analysis', $plugin_admin, 'yoast_admin_cols' );
		
		/* Remove ACF Menu for Manager Admins */
		$this->loader->add_filter( 'acf/settings/capability', $plugin_admin, 'acf_capability_setting' );
		
		/* Global DMS Theme Options */
		$this->loader->add_action( 'pagelines_setup', $plugin_admin, 'global_dms_options' );
		
		/* Disable/Enable DMS Section Modules via our options */
		$this->loader->add_action( 'after_theme_setup' , $plugin_admin, 'toggle_section_modules' );
		
		/* Limit Page Number */
		$this->loader->add_action( 'save_post_page', $plugin_admin, 'limit_pages', 10, 3 );
		$this->loader->add_action( 'admin_notices', $plugin_admin, 'page_limit_notice' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new K8_Admin_Public( $this->get_K8_Admin(), $this->get_version(), $this->K8_Admin_User_Roles );
										
		/* Update DMS toolbar config */
		$this->loader->add_filter( 'pl_sorted_toolbar_config', $plugin_public, 'update_dms_toolbar_config' );
		
		/* Filter Body Class */
		$this->loader->add_filter( 'body_class', $plugin_public, 'user_role_body_class' ); // User Role Class
		$this->loader->add_filter( 'body_class', $plugin_public, 'navi_logo_body_class' ); // Navi Logo Option Class
		$this->loader->add_filter( 'body_class', $plugin_public, 'ibox_square_image_body_class' ); // iBox Square Image Class
						
		/* Load restrict dms less file */
		$this->loader->add_filter( 'pagelines_lesscode', $plugin_public, 'restrict_dms_less' );
		$this->loader->add_filter( 'pl_pro_disable_class', $plugin_public, 'plpro_disable_class' ); // src - dms/includes/lib.editor.php line 78
		
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts', 35 );
		
		/* Replace DMS Loader */ 
		remove_action( 'pagelines_before_site', array( 'PageLinesPageLoader', 'loader_html' ), 25 );
		$this->loader->add_action( 'pagelines_before_site', $plugin_public, 'loader_html', 35 );		
						
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_K8_Admin() {
		return $this->K8_Admin;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    K8_Admin_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
