<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * Dashboard. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://kri8it.com
 * @since             1.0.0
 * @package           K8 Admin
 *
 * @wordpress-plugin
 * Plugin Name:       K8 Admin
 * Plugin URI:        http://kri8it.com/k8-admin-plugin/
 * Description:       Clean up admin for our users and different user roles.
 * Version:           1.0.7
 * Author:            Charl Pretorius
 * Author URI:        http://kri8it.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       k8-admin
 * Domain Path:       /languages
 * 
 * K8 Bitbucket Plugin URI: https://bitbucket.org/kri8it/k8-admin
 */


// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

//TODO find a better way to check if dms is available, not just parent theme
if ( wp_get_theme()->template != 'dms' ) {
	return;
}

/**
 * The code that runs during plugin activation.
 */
require_once plugin_dir_path( __FILE__ ) . 'includes/class-k8-admin-activator.php';

/**
 * The code that runs during plugin deactivation.
 */
require_once plugin_dir_path( __FILE__ ) . 'includes/class-k8-admin-deactivator.php';

/** This action is documented in includes/class-k8-admin-activator.php */
register_activation_hook( __FILE__, array( 'K8_Admin_Activator', 'activate' ) );

/** This action is documented in includes/class-k8-admin-deactivator.php */
register_deactivation_hook( __FILE__, array( 'K8_Admin_Deactivator', 'deactivate' ) );

/**
 * The core plugin class that is used to define internationalization,
 * dashboard-specific hooks, and public-facing site hooks.
 */
require_once plugin_dir_path( __FILE__ ) . 'includes/class-k8-admin.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_K8_Admin() {

	$plugin = new K8_Admin();
	$plugin->run();

}
run_K8_Admin();
