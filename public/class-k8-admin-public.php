<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://kri8it.com
 * @since      1.0.0
 *
 * @package    K8_Admin
 * @subpackage K8_Admin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the dashboard-specific stylesheet and JavaScript.
 *
 * @package    K8_Admin
 * @subpackage K8_Admin/public
 * @author     Charl Pretorius <charl@kri8it.com>
 */
class K8_Admin_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $K8_Admin    The ID of this plugin.
	 */
	private $K8_Admin;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;
	
	/**
	 * Restrict DMS Less file
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $restrict_dms_less    Restrict DMS Less file
	 */
	private $restrict_dms_less;
	
	/**
	 * The user role class of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      class    $K8_Admin_User_Roles    The user role class of this plugin.
	 */
	private $K8_Admin_User_Roles;
	
	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @var      string    $K8_Admin       The name of the plugin.
	 * @var      string    $version    The version of this plugin.
	 */
	public function __construct( $K8_Admin, $version, $K8_Admin_User_Roles ) {

		$this->K8_Admin = $K8_Admin;
		$this->version = $version;
		
		$this->K8_Admin_User_Roles = $K8_Admin_User_Roles;
		
		$this->restrict_dms_less = plugin_dir_url( __FILE__ ) . '/css/restrict_dms_editor.less';

	}
		
	/**
	 * Generate less to restrict and hide some dms funcitons 
	 *
	 * @since    1.0.0
	 * @var      string    $less       less code
	 * 
	 * @uses	pl_file_get_contents
	 */
	public function restrict_dms_less( $less ){
		
		//$less .= pl_file_get_contents( $this->restrict_dms_less );
		$less .= file_get_contents( $this->restrict_dms_less ); 
		return $less;

	}
	
	/**
	 * Use DMS Pro plugin filter to add hidden class 
	 *
	 * @since    1.0.0
	 * @var      string    $hidden       class
	 * 
	 * @uses	pl_file_get_contents
	 */
	public function plpro_disable_class( $hidden ){
		
		$hidden = 'hidden';
		
		return $hidden;
		
	}
	
	/**
	 * Update DMS toolbar config based on user role
	 *
	 * @since    1.0.0
	 */
	public function update_dms_toolbar_config( $toolbar_config ){		
		
		if( $this->K8_Admin_User_Roles->is_client_admin() ){ // client_admin role
		   
		    unset( $toolbar_config['add-new'] );
						
			unset( $toolbar_config['page-setup'] );
			
			unset( $toolbar_config['settings']['panel']['layout'] );
			unset( $toolbar_config['settings']['panel']['color_control'] );
			unset( $toolbar_config['settings']['panel']['typography'] );
			unset( $toolbar_config['settings']['panel']['poppy'] );
			unset( $toolbar_config['settings']['panel']['salebar'] );
			unset( $toolbar_config['settings']['panel']['importexport'] );
			unset( $toolbar_config['settings']['panel']['advanced'] );
			unset( $toolbar_config['settings']['panel']['resets'] );
		    
			//unset( $toolbar_config['pl-design'] );
			unset( $toolbar_config['dev'] );
			unset( $toolbar_config['pagelines-help'] );
			unset( $toolbar_config['pagelines-home'] );	
			
		}
		
		if( $this->K8_Admin_User_Roles->is_manager_admin() ){ // manager_admin role
			
			unset( $toolbar_config['add-new']['panel']['more_sections'] );
					
			unset( $toolbar_config['page-setup']['panel']['tmp_save'] );
		
			unset( $toolbar_config['settings']['panel']['importexport'] );
			unset( $toolbar_config['settings']['panel']['advanced'] );
			
			unset( $toolbar_config['dev']['panel']['secopts'] );
						
			unset( $toolbar_config['pagelines-help'] );
			unset( $toolbar_config['pagelines-home'] );	
		
		}
		
		return $toolbar_config;
		
	}
	
	/**
	 * Add current user's role classes
	 *
	 * @since    1.0.0
	 */
	public function user_role_body_class( $classes ) {
	
		$user = wp_get_current_user();
		
		foreach( $user->roles as $role ):	
			
			$classes[] = 'role-' . $role;
		    
		endforeach;
		
	    return $classes;
	}
	
	/**
	 * Add Navi section big logo option class
	 *
	 * @since    1.0.6
	 */
	public function navi_logo_body_class( $classes ) {
	
		if( pl_setting( 'k8_opts_logo_navi_logo_big' ) ) $classes[] = 'k8-navi-logo-big';
		
	    return $classes;
	}
	
	/**
	 * Add iBox Square Image option class
	 *
	 * @since    1.0.7
	 */
	public function ibox_square_image_body_class( $classes ) {
	
		if( pl_setting( 'k8_opts_ibox_square_thumbnails' ) ) $classes[] = 'k8-ibox-square-image';
		
	    return $classes;
	}
	
	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in K8_Admin_Public_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The K8_Admin_Public_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->K8_Admin, plugin_dir_url( __FILE__ ) . 'css/k8-admin-public.css', array(), $this->version, 'all' );

	}
			
	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in K8_Admin_Public_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The K8_Admin_Public_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->K8_Admin, plugin_dir_url( __FILE__ ) . 'js/k8-admin-public.js', array( 'jquery', 'pl-editor-js' ), $this->version, true );

	}
	
	/**
	 * Update the editor loader
	 *
	 * @since    1.0.0
	 */
	function loader_html(){
				
		if( $this->K8_Admin_User_Roles->get_current_user_role() !== 'other' ):
		
		?>
		<div class="pl-loader pl-loader-page pl-pro-version">
			<div class="loader-text" style="padding: 200px 0;font-family: 'Open Sans', helvetica, arial, sans-serif; font-size: 30px; line-height: 1.9em; font-weight: 600; letter-spacing: -1px; ">
				<div class="pl-spin-c pl-animation pla-from-top "><div class="pl-spinner"></div></div>
				<div class="pl-loader-head pl-animation pla-from-bottom "><?php _e('Loading Editor', 'pagelines');?></div>
				
				<script> 
				setTimeout(function() { jQuery(".pl-loader .pl-animation").addClass('animation-loaded') }, 150);				
				setTimeout(function() { jQuery(".pl-loader .pl-loader-head").html('Oops, There may be an issue loading. <div style="font-size: 16px; opacity: .5; line-height: 1.55em;">The editor typically loads in less than 5 seconds. Please check for Javascript or PHP errors.<br/>(Typically, slow or incomplete loading is related to plugin conflicts or server issues.)<br/><a href="http://kri8it.com" target="_blank">Contact Support</a></div>') }, 13000);				
				</script>
			</div>
		</div>
		
		<?php
		
		endif;
		
	}

}
